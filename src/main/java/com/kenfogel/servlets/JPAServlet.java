package com.kenfogel.servlets;

import com.kenfogel.beans.FishActionBeanJPA;
import com.kenfogel.entities.Fish;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kfogel
 */
@WebServlet(name = "JPAServlet", urlPatterns = {"/JPAServlet"})
public class JPAServlet extends HttpServlet {

    @Inject
    private FishActionBeanJPA fab;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        boolean success = true;
        List<Fish> fishies = null;

        try {
            fishies = fab.getAll();
        } catch (Exception ex) {
            Logger.getLogger(JPAServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(
                    "<!doctype html public \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "<title>JPA and Servlet</title>\n"
                    + "</head>\n");
            out.println("<p><TABLE RULES=all>");
            for (Fish f : fishies) {
                out.println("<tr>");
                out.print("<td>"
                        + f.getId()
                        + "</td><td>"
                        + f.getCommonname()
                        + "</td><td>"
                        + f.getLatin()
                        + "</td><td>"
                        + f.getPh()
                        + "</td><td>"
                        + f.getKh()
                        + "</td><td>"
                        + f.getTemp()
                        + "</td><td>"
                        + f.getFishsize()
                        + "</td><td>"
                        + f.getSpeciesorigin()
                        + "</td><td>"
                        + f.getTanksize()
                        + "</td><td>"
                        + f.getStocking()
                        + "</td><td>"
                        + f.getDiet()
                        + "</td>");
                out.println("</tr>");
            }
            out.println("</table></body></html>");
        }
    }
}
